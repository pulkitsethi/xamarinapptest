﻿using System;
using System.Threading.Tasks;
using App.Common.Core.ViewModels;
using Xamarin.Forms;

namespace App.Common.Core.Views
{
	public abstract class BaseView : ContentPage
    {
        public BaseView()
        {
			AppContainer.Instance.LoadViewModel(this);
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (null != BindingContext && BindingContext is BaseViewModel vm)
				Title = vm.Title;
		}
	}
}
