﻿using System;
using System.Collections.Generic;

namespace App.Common.Core.Models
{
	public class AnimeSearchModel
	{
		public bool request_cached { get; set; }
		public List<AnimeItem> result { get; set; }
    }
}
