﻿using System;

namespace App.Common.Core.Models
{
    public class AnimeItem
    {
		public string title { get; set; }
		public string description { get; set; }
		public string type { get; set; }
		public double Score { get; set; }
		public double episodes { get; set; }
		public double members { get; set; }
    }
}
