﻿using System;
namespace App.Common.Core.Helpers
{
    public class AppUrls
    {
		private const string BaseUrl = "https://api.jikan.moe/search/anime/";

        public static string AnimeSearchUrl(string searchText)
		{
			return $"{BaseUrl}{searchText}/1";
		}
	}
}
