﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using App.Common.Core.Services.App;
using App.Common.Core.Services.Network;
using Newtonsoft.Json;
using Xamarin.Forms;
using System.Reflection;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace App.Common.Core.Services.Mock
{
	public enum ServiceMode
    {
        RECORD,
        MOCK
    }

    public class Request
    {
		public string Uri { get; set; }
        public string RelativeUri { get; set; }
        public string Method { get; set; }
        public List<string> Parameters { get; set; }
    }

    public class Response
    {
		public string HttpResponse { get; set; }
		public string Content { get; set; }
		public int Delay { get; set; }
    }

    public class Recording
    {
		public Request Request { get; set; }
        public Response Response { get; set; }
    }

	public class Header
    {
        public string Key { get; set; }
        public IList<string> Value { get; set; }
    }

    public class Content
    {
        public IList<Header> Headers { get; set; }
    }

    public class Version
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
        public int Revision { get; set; }
        public int MajorRevision { get; set; }
        public int MinorRevision { get; set; }
    }

    public class ResponseMessageHttp
    {
        public Content Content { get; set; }
        public IList<Header> Headers { get; set; }
        public bool IsSuccessStatusCode { get; set; }
        public string ReasonPhrase { get; set; }
        public int StatusCode { get; set; }
        public Version Version { get; set; }
    }



    public class Recordings
	{
		public List<Recording> Data { get; set; }
	}

	public class MockNetworkService : INetworkService
	{
		const string _relativeUri = "/api/";
		const string _folderName = "Recordings";
		const string _fileName = "TestRecording.json";

		readonly IStorageService _storageService;

		private List<Recording> AllRecordings { get; set; }

		protected ServiceMode Mode { get; set; }

		private Recordings Recordings { get; set; }

		private readonly JsonSerializerSettings _serializerSettings;
        

		public MockNetworkService()
		{
			// Set mode
			Mode = ServiceMode.MOCK;

			Recordings = new Recordings
			{
				Data = new List<Recording>()
			};

			_storageService = DependencyService.Get<IStorageService>();

			// Cannot record with storage service => iOS
			if (null == _storageService)
				Mode = ServiceMode.MOCK;

			_serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            };

			//LoadInteractionFromFile("TestRecording");
		}

		private static readonly Lazy<HttpClient> lazyClient = new Lazy<HttpClient>(() => CreateHttpClient());

		private static HttpClient CreateHttpClient()
		{
			var client = new HttpClient();
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			return client;
		}

		public async Task<ServiceResult<TResult>> GetAsync<TResult>(string uri, string token = "")
		{
			var method = "GET";
			ServiceResult<TResult> result = new ServiceResult<TResult>
			{
				Data = default(TResult),
				IsError = true,
			};

			try
			{
				if (ServiceMode.RECORD == Mode)
				{
					var recording = new Recording
					{
						Request = GetRequest(uri, method)
					};

					recording.Request.Method = method;

					recording.Response = new Response();
					var response = await lazyClient.Value.GetAsync(uri);

					// Serialize response
					recording.Response.HttpResponse = JsonConvert.SerializeObject(response);

                    if (response.IsSuccessStatusCode)
                    {
                        result.IsError = false;

						var content = await response.Content.ReadAsStringAsync();
						recording.Response.Content = content;
						recording.Response.Delay = 2000;

						result.Data = JsonConvert.DeserializeObject<TResult>(content);
                    }
                    else
                    {
                        result.ErrorCode = (int)response.StatusCode;
                        result.ErrorMessage = response.ReasonPhrase;
                    }

					// Save current recording
					await InsertRecording(method, recording);
				}
				else if (ServiceMode.MOCK == Mode)
				{
					Recording savedRecording = null;

					foreach (var r in Recordings.Data)
					{
						if (MatchRequest(method, r.Request, GetRequest(uri, method)))
						{
							savedRecording = r;
							break;
						}
					}

					await Task.Delay(savedRecording.Response.Delay);

					var response = JsonConvert.DeserializeObject<ResponseMessageHttp>(savedRecording.Response.HttpResponse, _serializerSettings);
                    
					if (response.IsSuccessStatusCode)
                    {
                        result.IsError = false;
						result.Data = JsonConvert.DeserializeObject<TResult>(savedRecording.Response.Content);
                    }
                    else
                    {
                        result.ErrorCode = (int)response.StatusCode;
                        result.ErrorMessage = response.ReasonPhrase;
                    }
				}
			}
			catch (Exception exp)
			{
				result.IsError = true;
				result.ErrorCode = (int)HttpStatusCode.InternalServerError; // Unknown
				result.ErrorMessage = exp.Message;
			}

			return result;
		}



		public Task DeleteAsync(string uri, string token = "")
		{
			throw new NotImplementedException();
		}

		public Task<ServiceResult<bool>> PatchAsync(string uri, object data, string token = "", string header = "")
		{
			throw new NotImplementedException();
		}

		public Task<ServiceResult<TResult>> PostAsync<TResult>(string uri, TResult data, string token = "", string header = "")
		{
			throw new NotImplementedException();
		}

		public Task<ServiceResult<TResult>> PostAsync<TResult>(string uri, string data, string clientId, string clientSecret)
		{
			throw new NotImplementedException();
		}

		public Task<ServiceResult<TResult>> PutAsync<TResult>(string uri, TResult data, string token = "", string header = "")
		{
			throw new NotImplementedException();
		}

		#region Implementation

        public void LoadApiData(string jsonData)
		{
			LoadMockData(jsonData, false);
        }

		public void ReloadApiData(string jsonData)
        {
            LoadMockData(jsonData, true);
        }

		private async Task InsertRecording(string method, Recording recording)
        {
            Recordings allRecordings = null;
            var currentRecordings = await _storageService.GetContent(_folderName, _fileName);

            if (null != currentRecordings)
            {
                allRecordings = JsonConvert.DeserializeObject<Recordings>(currentRecordings);
            }
            else
                allRecordings = new Recordings { Data = new List<Recording>() };

            var toRemove = allRecordings.Data.Where(x => MatchRequest(method, x.Request, recording.Request)).FirstOrDefault();
            if (null != toRemove)
                allRecordings.Data.Remove(toRemove);

            allRecordings.Data.Add(recording);

            var content = JsonConvert.SerializeObject(allRecordings);
            await _storageService.SaveFile(_folderName, _fileName, content);
        }

        private bool MatchRequest(string method, Request first, Request second)
        {
            if (0 == string.Compare(second.Method, method, StringComparison.CurrentCultureIgnoreCase)
                && (0 == string.Compare(first.Uri, second.Uri, StringComparison.CurrentCultureIgnoreCase)))
            {
                if (null == first.Parameters && null == second.Parameters)
                    return true;
                if (null == first.Parameters && null != second.Parameters || null != first.Parameters && null != second.Parameters)
                    return false;
                if (null != first.Parameters && null != second.Parameters && first.Parameters.Intersect(second.Parameters).Count() == second.Parameters.Count())
                    return true;
            }

            return false;
        }

        private Request GetRequest(string uri, string method)
        {
            var decodedUri = HttpUtility.UrlDecode(uri);
            string relativeUriPath = string.Empty;
            List<string> queryParams = null;

            if (decodedUri.ToLower().Contains(_relativeUri))
            {
                if (decodedUri.Contains('?'))
                {
                    var startIndex = decodedUri.IndexOf(_relativeUri, StringComparison.CurrentCultureIgnoreCase);
                    var endIndex = decodedUri.IndexOf('?');
                    relativeUriPath = decodedUri.Substring(startIndex, endIndex - startIndex);
                }
                else
                {
                    relativeUriPath = decodedUri.Substring(decodedUri.IndexOf(_relativeUri, StringComparison.CurrentCultureIgnoreCase));
                }
            }
            if (decodedUri.Contains('?'))
            {
                var queryString = decodedUri.Substring(decodedUri.IndexOf('?')).Split('#')[0];
                var decodeParams = HttpUtility.ParseQueryString(queryString);

                queryParams = new List<string>(decodeParams.AllKeys);
            }

            return new Request
            {
                Uri = decodedUri,
                Parameters = queryParams,
                RelativeUri = relativeUriPath,
                Method = method
            };
        }

        private void LoadMockData(string jsonData, bool replace)
        {
			if (null != Recordings.Data && replace)
				Recordings.Data.Clear();

			var recordings = JsonConvert.DeserializeObject<Recordings>(jsonData);

			if (null != recordings && null != recordings.Data)
				Recordings.Data.AddRange(recordings.Data);
		}

        private void LoadInteractionFromFile(String fileName)
		{
			var assembly = Assembly.GetExecutingAssembly();
            Stream stream = assembly.GetManifestResourceStream($"App.Common.Core.Services.Mock.Interactions.{fileName}.json");

            using (var streamReader = new StreamReader(stream))
            {
                // Note: ReadToEnd returns null
                StringBuilder jsonBuffer = new StringBuilder();

                string nextLine = null;
                while ((nextLine = streamReader.ReadLine()) != null)
                    jsonBuffer.Append(nextLine);

				LoadMockData(jsonBuffer.ToString(), false);
            }
		}

		#endregion
	}
}
