﻿using System;
using System.Threading.Tasks;

namespace App.Common.Core.Services.App
{
    public interface INavigationService
    {
		Task NavigateToAsync<TViewModel>(object parameter);
    }
}
