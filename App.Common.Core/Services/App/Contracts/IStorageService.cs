﻿using System;
using System.Threading.Tasks;

namespace App.Common.Core.Services.App
{
    public interface IStorageService
    {
		Task SaveFile(string path, string fileName, string content);
		Task<string> GetContent(string folderName, string fileName);
    }
}
