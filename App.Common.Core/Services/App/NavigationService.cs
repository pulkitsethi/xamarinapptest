﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using App.Common.Core.ViewModels;
using Xamarin.Forms;

namespace App.Common.Core.Services.App
{
	public class NavigationService : INavigationService
    {
        public NavigationService()
        {
        }

		public async Task NavigateToAsync<TViewModel>(object parameter)
		{
			var vmType = typeof(TViewModel);
			
			var viewType = Type.GetType($"{vmType.FullName.Replace("Model", string.Empty)}, {vmType.GetTypeInfo().Assembly.FullName}");
			
			Page page = Activator.CreateInstance(viewType) as Page;

			if(page.BindingContext is BaseViewModel vm)
				await vm.InitAsync(parameter);

			await Application.Current.MainPage.Navigation.PushAsync(page, true);
		}
    }
}
