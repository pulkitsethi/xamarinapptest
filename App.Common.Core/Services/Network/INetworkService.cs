﻿using System;
using System.Threading.Tasks;

namespace App.Common.Core.Services.Network
{
    public interface INetworkService
    {
		Task<ServiceResult<TResult>> GetAsync<TResult>(string uri, string token = "");

        Task<ServiceResult<TResult>> PostAsync<TResult>(string uri, TResult data, string token = "", string header = "");

        Task<ServiceResult<TResult>> PostAsync<TResult>(string uri, string data, string clientId, string clientSecret);

        Task<ServiceResult<TResult>> PutAsync<TResult>(string uri, TResult data, string token = "", string header = "");

        Task<ServiceResult<bool>> PatchAsync(string uri, object data, string token = "", string header = "");

        Task DeleteAsync(string uri, string token = "");
    }

	public class ServiceResult<T>
    {
        public int ErrorCode
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            get;
            set;
        }

        public bool IsError
        {
            get;
            set;
        }

        public T Data
        {
            get;
            set;
        }
    }
}
