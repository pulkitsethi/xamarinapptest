﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace App.Common.Core.Services.Network
{
	public class NetworkService : INetworkService
	{
		private static readonly Lazy<HttpClient> lazyClient = new Lazy<HttpClient>(() => CreateHttpClient());
        
		private static HttpClient CreateHttpClient()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

		public NetworkService()
		{
		}

		public Task DeleteAsync(string uri, string token = "")
		{
			throw new NotImplementedException();
		}

		public async Task<ServiceResult<TResult>> GetAsync<TResult>(string uri, string token = "")
		{
			ServiceResult<TResult> result = CreateResponseData<TResult>();

			var client = lazyClient.Value;
			var response = await client.GetAsync(uri);

			if(response.IsSuccessStatusCode)
			{
				string serialized = await response.Content.ReadAsStringAsync();
				result.Data = JsonConvert.DeserializeObject<TResult>(serialized);
			}

			return result;
		}

		public Task<ServiceResult<bool>> PatchAsync(string uri, object data, string token = "", string header = "")
		{
			throw new NotImplementedException();
		}

		public Task<ServiceResult<TResult>> PostAsync<TResult>(string uri, TResult data, string token = "", string header = "")
		{
			throw new NotImplementedException();
		}

		public Task<ServiceResult<TResult>> PostAsync<TResult>(string uri, string data, string clientId, string clientSecret)
		{
			throw new NotImplementedException();
		}

		public Task<ServiceResult<TResult>> PutAsync<TResult>(string uri, TResult data, string token = "", string header = "")
		{
			throw new NotImplementedException();
		}

		private ServiceResult<TResult> CreateResponseData<TResult>()
        {
			return new ServiceResult<TResult>()
            {
                Data = default(TResult),
                ErrorCode = 0,
                ErrorMessage = string.Empty
            };
        }
	}
}
