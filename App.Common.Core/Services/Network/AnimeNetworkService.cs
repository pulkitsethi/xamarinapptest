﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Common.Core.Helpers;
using App.Common.Core.Models;

namespace App.Common.Core.Services.Network
{
	public class AnimeNetworkService : IAnimeNetworkService
    {
		readonly INetworkService _networkService;
		public AnimeNetworkService(INetworkService networkService)
        {
			_networkService = networkService;
        }

		public async Task<List<AnimeItem>> SearchAnimesByKeyword(string searchText)
		{
			var url = AppUrls.AnimeSearchUrl(searchText);

			var response = await _networkService.GetAsync<AnimeSearchModel>(url);

			if (!response.IsError)
				return response.Data.result;

			return null;
		}
	}
}
