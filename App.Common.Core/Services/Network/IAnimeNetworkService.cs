﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Common.Core.Models;

namespace App.Common.Core.Services.Network
{
    public interface IAnimeNetworkService
    {
		Task<List<AnimeItem>> SearchAnimesByKeyword(string searchText);
    }
}
