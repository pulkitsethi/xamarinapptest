﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using App.Common.Core.Models;
using App.Common.Core.Services.Network;

namespace App.Common.Core.Services.Data
{
	public class AnimeService : IAnimeService
    {
		readonly IAnimeNetworkService _animeNetworkService;

		public AnimeService(IAnimeNetworkService animeNetworkService)
        {
			_animeNetworkService = animeNetworkService;
        }

		public async Task<List<AnimeItem>> SearchAnimes(string searchText)
		{
			var result = await _animeNetworkService.SearchAnimesByKeyword(searchText);

			if (null == result)
			{
				// TODO: return from local cache / offline database
				// Check if can return from local cache
				return null;
			}
			else
				return result;
		}

    }
}
