﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using App.Common.Core.Models;

namespace App.Common.Core.Services.Data
{
    public interface IAnimeService
    {
		Task<List<AnimeItem>> SearchAnimes(string searchText);
    }
}
