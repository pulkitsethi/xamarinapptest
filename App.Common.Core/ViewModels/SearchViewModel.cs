﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using App.Common.Core.Services.App;
using Xamarin.Forms;

namespace App.Common.Core.ViewModels
{
	public class SearchViewModel : BaseViewModel
	{
		#region readonly
		readonly INavigationService _navigationService;
		#endregion

		#region Private members
		private string _searchText = String.Empty;
		private bool _showBusy = false;
		private bool _searchEnabled = false;
		#endregion

		#region Commands

		public ICommand SearchCommand => new Command(async () => await BeginSearch());

		#endregion

		#region Properties

		public string SearchText
		{
			get
			{
				return _searchText;
			}
			set
			{
				_searchText = value;

				if (null != _searchText && _searchText.Length > 2)
					SearchEnabled = true;
				else
					SearchEnabled = false;
				
				RaisePropertyChanged(() => SearchText);
			}
		}

		public bool ShowBusy
		{
			get
			{
				return _showBusy;
			}
			set
			{
				_showBusy = value;
				RaisePropertyChanged(() => ShowBusy);
			}
		}

		public bool SearchEnabled
        {
            get
            {
				return _searchEnabled;
            }
            set
            {
				_searchEnabled = value;
				RaisePropertyChanged(() => SearchEnabled);
            }
        }

		#endregion

		#region Constructor

		public SearchViewModel(INavigationService navigationService)
		{
			_navigationService = navigationService;

			Title = "Search anime";
		}

		#endregion

		#region Implementation

		private async Task BeginSearch()
		{
			ShowBusy = true;

			await _navigationService.NavigateToAsync<AllItemsViewModel>(SearchText);

			ShowBusy = false;
		}

		#endregion
	}
}
