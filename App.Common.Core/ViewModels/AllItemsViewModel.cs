﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Common.Core.Models;
using App.Common.Core.Services.Data;

namespace App.Common.Core.ViewModels
{
	public class AllItemsViewModel : BaseViewModel
    {

        #region readonly

		readonly IAnimeService _animeService;

		#endregion

		#region Private members

		private List<AnimeItem> _itemList;

		#endregion

		#region Properties

		public List<AnimeItem> ItemList
		{
			get
			{
				return _itemList;
			}
			set
			{
				_itemList = value;
				RaisePropertyChanged(() => ItemList);
			}
		}

		#endregion



		public AllItemsViewModel(IAnimeService animeService)
        {
			_animeService = animeService;

			Title = "Anime list";
        }

		public override async Task InitAsync(object parameter)
		{
			ItemList = await _animeService.SearchAnimes(parameter.ToString());
		}
	}
}
