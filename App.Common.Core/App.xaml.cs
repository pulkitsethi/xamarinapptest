﻿using System;
using System.Threading.Tasks;
using App.Common.Core;
using App.Common.Core.Views;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace App.TestDemo
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			MainPage = new NavigationPage(new SearchView());

            #if AUTOMATION

			Task.Run(async () =>
			{
				var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Storage, Permission.Location });
			});

            #endif
        }


    }
}
