﻿using System;
using App.UI.Tests.Views;
using NUnit.Framework;
using Xamarin.UITest;

namespace App.UI.Tests.Tests
{
	[TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
	public class ItemListTest : BaseTest
	{
		public ItemListTest(Platform platform) : base(platform)
		{ }

		[Test]
		public void ItemListBasicTest()
		{
			LoadApiData("TestRecording");

			var searchScreen = new SearchScreenView(_app);

			var itemList = searchScreen.
								EnterSearchTerm("ram").
								StartSearch().
								MoveToAnimeListView();
		}
	}
}
