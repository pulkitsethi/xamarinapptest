﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using NUnit.Framework;
using Xamarin.UITest;

namespace App.UI.Tests.Tests
{
    public class BaseTest
    {
		const string AndroidApkPath = "../../../Droid/bin/Automation/App.Base.App_TestDemo.apk";

		const string MethodApiDataLoad = "LoadApiData";
  
		public IApp _app;

        protected Platform RunningPlatform;
        protected readonly string _deviceSerial = "";
        //protected readonly string _deviceSerial = "6FC3FEF9-0E7D-414C-A60A-2C995E1703BF"; // IOS Simulator
        protected readonly string _deviceIPAddress = "";

        public BaseTest(Platform platform)
        {
            RunningPlatform = platform;
        }

		[SetUp]
        public virtual void SetUp()
        {
            if (null == _app)
				_app = AppInitializer.StartApp(RunningPlatform, _deviceSerial, _deviceIPAddress, AndroidApkPath, true);
        }

		public void LoadApiData(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            Stream stream = assembly.GetManifestResourceStream($"App.UI.Tests.Interactions.{fileName}.json");

            using (var streamReader = new StreamReader(stream))
            {
                // Note: ReadToEnd returns null
                StringBuilder jsonBuffer = new StringBuilder();

                string nextLine = null;
                while ((nextLine = streamReader.ReadLine()) != null)
                    jsonBuffer.Append(nextLine);

				CallBackDoorMethod(MethodApiDataLoad, jsonBuffer.ToString());
            }
        }

        private void CallBackDoorMethod(string methodName, object args)
        {
            if (RunningPlatform == Platform.Android)
                _app.Invoke(methodName, args);
            else if (RunningPlatform == Platform.iOS)
                _app.Invoke($"{methodName}:", new object[] { args });
        }
    }
}
