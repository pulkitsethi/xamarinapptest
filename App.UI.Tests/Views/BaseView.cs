﻿using System;
using Xamarin.UITest;

namespace App.UI.Tests.Views
{
    public class BaseView
    {
		protected IApp _app;

        public BaseView(IApp app)
        {
			_app = app;
        }
    }
}
