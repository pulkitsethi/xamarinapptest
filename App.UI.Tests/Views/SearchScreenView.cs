﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace App.UI.Tests.Views
{
	public class SearchScreenView : BaseView
    {
		Func<AppQuery, AppQuery> _searchEdit = s => s.Marked("searchText");
		Func<AppQuery, AppQuery> _searchBtn = s => s.Marked("searchBtn");

		public SearchScreenView(IApp app) : base(app)
        {
        }

		public SearchScreenView EnterSearchTerm(string text)
		{
			_app.EnterText(_searchEdit, text);

			return this;
		}

		public SearchScreenView StartSearch()
		{
			_app.Tap(_searchBtn);

			return this;
		}

		public ItemListView MoveToAnimeListView()
		{
			return new ItemListView(_app);
		}
    }
}
