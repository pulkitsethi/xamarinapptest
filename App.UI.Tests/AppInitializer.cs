﻿using System;
using System.IO;
using System.Linq;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace App.UI.Tests
{
	public class AppInitializer
	{
		const string AppBundle = "App.Base.TestDemo";

		public static IApp StartApp(Platform platform, string deviceSerial, string deviceIPAddress, string androidApkPath, bool installApp)
		{
			switch (TestEnvironment.Platform)
			{
				case TestPlatform.Local:
					return ConnectToApp(platform, deviceSerial, deviceIPAddress, androidApkPath, installApp);
				case TestPlatform.TestCloudAndroid:
					return ConfigureApp.Android.StartApp();
				case TestPlatform.TestCloudiOS:
					return ConfigureApp.iOS.StartApp();
			}

			throw new Exception("Invlaid platform");
		}

		private static IApp ConnectToApp(Platform platform, string deviceSerial, string deviceIPAddress, string androidApkPath, bool installApp)
		{
			if (Platform.Android == platform)
			{
				return installApp
					? ConfigureApp.Android.ApkFile(androidApkPath).DeviceSerial(deviceSerial).StartApp()
									  : ConfigureApp.Android.ApkFile(androidApkPath).DeviceSerial(deviceSerial).Debug().ConnectToApp();
			}
			else
			{
				var iOS = ConfigureApp.iOS;

				if (!string.IsNullOrEmpty(deviceSerial))
					iOS = iOS.DeviceIdentifier(deviceSerial);

				if (!string.IsNullOrEmpty(deviceIPAddress))
					iOS = iOS.DeviceIp(deviceIPAddress);

				iOS = iOS.InstalledApp(AppBundle);

				return installApp ? iOS.Debug().StartApp() : iOS.Debug().ConnectToApp();
			}
		}
	}

}
