﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Common.Core;
using App.Common.Core.Services.Mock;
using App.Common.Core.Services.Network;
using Foundation;
using UIKit;

namespace App.TestDemo.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
#if AUTOMATION


#endif
			Xamarin.Calabash.Start();
			global::Xamarin.Forms.Forms.Init();
			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}

#if AUTOMATION

		[Export("LoadApiData:")]
		public NSString LoadApiData(NSString jsonData)
		{
			var rp = AppContainer.Instance.Resolve<INetworkService>() as MockNetworkService;
			rp.LoadApiData(jsonData);

			return new NSString();
		}

		[Export("ReloadApiData:")]
		public NSString ReloadApiData(NSString jsonData)
        {
            var rp = AppContainer.Instance.Resolve<INetworkService>() as MockNetworkService;
			rp.ReloadApiData(jsonData);

            return new NSString();
        }

#endif
	}
}
