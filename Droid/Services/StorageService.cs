﻿using System;
using Xamarin.Forms;
using App.Common.Core.Services.App;
using App.TestDemo.Droid.Services;
using System.Threading.Tasks;
using System.IO;

[assembly: Dependency(typeof(StorageService))]
namespace App.TestDemo.Droid.Services
{
	public class StorageService : IStorageService
    {
        public StorageService()
        {
        }

		public async Task SaveFile(string folder, string fileName, string content)
		{
			string path = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, Android.OS.Environment.DirectoryDocuments + "/TestDemo");
            path = Path.Combine(path, folder);

            var info = Directory.CreateDirectory(path);

            string filePath = Path.Combine(path, fileName);

            using (var streamWriter = new StreamWriter(filePath, false))
            {
                await streamWriter.WriteAsync(content);
            }
		}

		public async Task<string> GetContent(string folderName, string fileName)
        {
            string path = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, Android.OS.Environment.DirectoryDocuments + "/TestDemo");
            path = Path.Combine(path, folderName);

            string filePath = Path.Combine(path, fileName);

            if (!File.Exists(filePath))
                return null;

            using (var streamReader = new StreamReader(filePath))
            {
                return await streamReader.ReadToEndAsync();
            }
        }

	}
}
