using System;

using Android.App;
using Android.OS;
using Android.Runtime;
using App.Common.Core;
using App.Common.Core.Services.Mock;
using App.Common.Core.Services.Network;
using Java.Interop;
using Plugin.CurrentActivity;

namespace App.TestDemo.Droid
{
	//You can specify additional application information in this attribute
    [Application]
    public class MainApplication : Application, Application.IActivityLifecycleCallbacks
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transer)
          :base(handle, transer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            RegisterActivityLifecycleCallbacks(this);
            //A great place to initialize Xamarin.Insights and Dependency Services!
        }

        public override void OnTerminate()
        {
            base.OnTerminate();
            UnregisterActivityLifecycleCallbacks(this);
        }

        public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityDestroyed(Activity activity)
        {
        }

        public void OnActivityPaused(Activity activity)
        {
        }

        public void OnActivityResumed(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
        {
        }

        public void OnActivityStarted(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityStopped(Activity activity)
        {
        }

		#if AUTOMATION

        [Export("LoadApiData")]
		public void LoadApiData(string jsonData)
        {
            var rp = AppContainer.Instance.Resolve<INetworkService>() as MockNetworkService;
            rp.LoadApiData(jsonData);
        }

        [Export("ReloadApiData")]
		public void ReloadApiData(string jsonData)
        {
            var rp = AppContainer.Instance.Resolve<INetworkService>() as MockNetworkService;
            rp.ReloadApiData(jsonData);
        }

        #endif
    }
}